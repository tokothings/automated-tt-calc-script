// ==UserScript==
// @name             Automated TT Calc Script (Atcas)
// @match						 https://www.deviantart.com/comments/1/692741809/4431867623
// @match						 https://www.deviantart.com/comments/1/692741809/4431867690
// @match						 https://www.deviantart.com/comments/1/692741809/4431867760
// @match						 https://www.deviantart.com/comments/1/692741809/4431867821
// @match						 https://www.deviantart.com/comments/1/692741809/4431867880
// @match						 https://tokotna.com/calc/
// @version          1.0
// @grant 					 none
// @author					 majfisch @dA
// @description			 Automatically fills out TT Calculator and (todo) gets your RE redeems ready
// @require 				 https://code.jquery.com/jquery-3.6.0.min.js
// @run-at					 document-idle
// ==/UserScript==

// dA part
//
function buildResultsDiv(data) {
  // build div to copy data from
  const dataViewer = $("<div>");
  	//.text(preppedData)
  const resultsText = $("<div>");
  resultsText.text(JSON.stringify(data));
  dataViewer.css({position: "fixed",
                  border: "3px solid green",
                  width: "90%",
                  height: "90%",
                  overflow: "scroll",
                  top: "20px",
                  right: "20px",
                  "z-index": "3000",
                  "background-color": "#ccc"});
  dataViewer.attr("id", "atcasdataviewer");
  resultsText.attr("id", "atcasresults");
  resultsText.css("overflow", "scroll");
  $(resultsText).click(function(){selectText("atcasresults");});
  dataViewer.append(resultsText);
  $("body").prepend(dataViewer);
  // button to close div
  dataViewer.append($("<button>").html("X").click(()=> {
    dataViewer.css("visibility", "hidden");
  }).css({position: "fixed", bottom: "20px", right:"20px"}));
}

  // button to start script
const startButton= $("<button>");
startButton.addClass("_3X1Bu _6q2z_ Y0fGE y9NUd _2gmgj _2sHIK");
startButton.html("Run a.t.ca.s. Step 1");
startButton.attr("title", "Prepare Data from comments here for automatic insertion by atcas into the TT Calculator");
startButton.click(atcas);
$("[data-hook=comment_footer]").first().append(startButton);

function atcas() {
  // check if it has been run already
  if ($("#atcasdataviewer").length !== 0) {
    $("#atcasdataviewer").css("visibility", "visible");
    console.log("just showing again");
  } else {
      // get data from dA-comments
    const commentBoxes = $("[data-hook=comments_thread_item]");
    // filter and parse
    let preppedData = [];
    commentBoxes.each((index, comment) => {
      // filter for the ones that are AR results
      let commentTextDiv = $(comment).find("[data-id=rich-content-viewer]:contains('had a successful')");
      console.log(commentTextDiv.length);	
      console.log($(comment).attr("class"));
      if (commentTextDiv.length === 1){
        // clear out redeemed
        // look if there are comments in the same thread below the AR result
        if($(comment).next().attr("class").slice(0, 7) === $(comment).attr("class").slice(0, 7)) {
          $(comment).find("[data-hook=comment_footer]").append("this has no replies");
          preppedData.push(extractRedeemData(comment));
          console.log(preppedData);
        } else {
          $(comment).find("[data-hook=comment_footer]").append("this has replies");
          // select the elements between this and the next with the same classes
          // search them for "redeemed", but not "fp" in front
          if (searchSublevel(comment, "^redeemed")) {
            $(comment).find("[data-hook=comment_footer]").append(", redeemed!");
          } else {
            // must check for RE reveals/completes
            let redeemData = extractRedeemData(comment, true);
            if(redeemData!== null) {
              preppedData.push(redeemData);
            }
          }
        }
      }
    });

    console.log(preppedData);

    buildResultsDiv(preppedData);
  }


}

function extractRedeemData(comment, checkForReReply = false){
  $(comment).find("[data-hook=comment_footer]").append(", extracting...");
	// prepare dataset by extracting RE, RE status,
	// items, Tokota and comment link
  let redeemData = {};

  // check if roll had an RE
  if ($(comment).find("a:contains('random event')").length === 0) {
    $(comment).find("[data-hook=comment_footer]").append(", no RE");
    redeemData.re = -1; // no re
  } else {
    $(comment).find("[data-hook=comment_footer]").append(", RE");
    // see if Re is completed or revealed
    if (checkForReReply === true) {
    	// select the elements between this and the next with the same classes
      // search them for RE keywords
      if(searchSublevel(comment, "revealed")) {
        redeemData.re = 1; // re revealed
        $(comment).find("[data-hook=comment_footer]").append(" revealed");
        // revealed, also completed?
        if(searchSublevel(comment, "completed")) {
          // check for double or void
          if(searchSublevel(comment, "double")) {
            $(comment).find("[data-hook=comment_footer]").append("value doubled");
            redeemData.re = 2;
          } else if (searchSublevel(comment, "void")) {
          	return null;
          } else {
            redeemData.re = -1; // same as no RE
            $(comment).find("[data-hook=comment_footer]").append(" and completed");
          }
        }

      } else {
        $(comment).find("[data-hook=comment_footer]").append(" unrevealed");
      }
    } else {
      redeemData.re = 0; // re not revealed
    }
  }
  const boxHeader = $(comment).find("[data-commentid]");
  redeemData.link = "https://www.deviantart.com/comments/" +
    boxHeader.attr("data-typeid") + "/" +
    boxHeader.attr("data-itemid") + "/" +
    boxHeader.attr("data-commentid");
  console.log(redeemData.link);

  // get text element
  // const commentTextContainer = $(comment).find("#viewer-foo");
  const commentTextContainer = $(comment).find("[data-id=rich-content-viewer]");

  // get items
  let linkTexts = "";
  $(commentTextContainer).find("a").text((index, text) => {
    // don't use first link, it's the piece name
    if  (index > 0) {
      linkTexts += text + " -- ";
    }
    return text;
  	});
  console.log(linkTexts);
  redeemData.items = linkTexts;

  // get TokoID
  const tokoIdRegEx = new RegExp("\\<strong\\>.*[^0-9](\\d{3,6})\\<\\/strong\\>", "i");
  redeemData.tokoId = $(commentTextContainer).html().match(tokoIdRegEx)[1];

  // get activity type
  const actTypeRegEx = new RegExp("had a successful ([a-z ]+)!", "i");
  console.log(actTypeRegEx);
  let actType = "";
  const actMatched = $(commentTextContainer).text().match(actTypeRegEx);
  if (actMatched !== null) {
    actType = actMatched[1];
  }
  console.log(actMatched);
  console.log(actType);
	if (actType === "hunting trip") {
    redeemData.trait = "Hunter";
  } else if (actType === "fishing trip") {
    redeemData.trait = "Fisher";
  } else if (actType === "expedition") {
    redeemData.trait = "Explorer";
  } else if (actType === "caving trip") {
    redeemData.trait = "Caver";
  } else if (actType === "diving trip") {
    redeemData.trait = "Diver";
  }
  else {
    redeemData.trait = "";
  }
  console.log(redeemData);
  return redeemData;
}

function searchSublevel(comment, searchTerm){
  let sublevel = true;
  let currentComment = $(comment).next();
  while (sublevel) {
    if (isSameLevel(comment, currentComment)) {
      sublevel = false;
    } else {
      console.log("1 on sublevel");
      if (commentTextContains(currentComment, searchTerm)) {
        return true;
      }
      currentComment = $(currentComment).next();
    }
  }
  return false;
}

function isSameLevel(comment1, comment2) {
  if ($(comment1).attr("class").slice(0, 7) === $(comment2).attr("class").slice(0, 7)) {
    return true;
  } else {
    return false;
  }
}

function commentTextContains(comment, searchTerm) {
  const searchTermRegEx = new RegExp(searchTerm, 'i');
  if (searchTermRegEx.test($(comment).find("[data-id=rich-content-viewer]").text())) {
    console.log("found searchterm: " +searchTerm + " in " + $(comment).find("[data-id=rich-content-viewer]").text());
    return true;
  } else {
    return false;
  }
}

  // send them to TT Calc
   //perhaps manual copy pasting is safest

// TT Calc part

	// button to start
const ttCalcButton = $("<button>").text("New Atcas").addClass("button").addClass("content");
ttCalcButton.click(function() {atcasTokotna();});
const atcasDiv = $("<div>").append(ttCalcButton);
$("#content").prepend(atcasDiv);

// button to continue
if (sessionStorage.hasOwnProperty("atcasRolls")) {
  const ttCalcButtonCont = $("<button>").text("Continue Atcas").addClass("button").addClass("content");
	ttCalcButtonCont.click(function() {
    processRolls(true);
  });
  atcasDiv.prepend(ttCalcButtonCont);
  console.log("got here");
}

selectText("innerResults");


// button to edit settings like traits or totems (localstorage)

function atcasTokotna() {

  // build div and input field
  const dataInput = $("<div>");
  	//.text(preppedData)
  const resultsText = $("<textarea>");
  dataInput.css({position: "fixed",
                  border: "3px solid green",
                  width: "90%",
                  height: "90%",
                  overflow: "scroll",
                  top: "20px",
                  right: "20px",
                  "z-index": "3000",
                  "background-color": "#ccc"});
  dataInput.attr("id", "atcasdatainput");
  resultsText.attr("id", "atcasresults");
  resultsText.css("overflow", "scroll");
  dataInput.append(resultsText);
  $("body").prepend(dataInput);
  // button to start
  dataInput.append($("<button>").html("Go!").click(()=> {
    processRolls();
    dataInput.css("visibility", "hidden");
  }).css({position: "fixed", bottom: "20px", right:"50px"}));
  // button to close div
  dataInput.append($("<button>").html("X").click(()=> {
    dataInput.css("visibility", "hidden");
  }).css({position: "fixed", bottom: "20px", right:"20px"}));


}

function processRolls(isContinued) {

  console.log("processing");
  const settings = getSettings();
  console.log("settings");
  let rollResults = {};
  if (isContinued) {
    console.log("continuing");
    // get Data from Session Storage
    rollResults = JSON.parse(sessionStorage.getItem("atcasRolls"));
  } else {
    console.log("new");
    // get data from input field and put into obj.
    rollResults = JSON.parse($("#atcasresults").val());
    console.log(rollResults);
    // clear out non-completed REs (if specified, if data gotten from input)
        // this is not done before to have settings all in one place
    let rollResultsRe = [];
    rollResults.forEach(((result) => {
      // keep if re status matches those in settings
      if (settings.redeemRe.includes(result.re)) {
        rollResultsRe.push(result);
      }
    }));
    rollResults = rollResultsRe;
  }

  	
  console.log(rollResults);

	// process one batch of 5 comments
	// clear form
	for (let i = 0; i < 5; i++) {
    fillOutOne(i+1, {}, false);
  }
  // fill out form
  let nextBatch = rollResults.length;
  if (nextBatch > 5) { nextBatch = 5; }
  for (let i = 0; i < nextBatch; i++) {
    fillOutOne(i+1, rollResults[i], settings);
  }
  // cut used results from data
  rollResults.splice(0, 5);
  // store rest for next
  if ( rollResults.length > 0) {
    const atcasRolls = JSON.stringify(rollResults);
    sessionStorage.setItem("atcasRolls", atcasRolls);
    console.log("atcasRolls:");
    console.log(atcasRolls);
  } else {
    // clean up sessionstorage
    sessionStorage.removeItem("atcasRolls");
  }

    // click generate (if specified)
  if (settings.autogen) {
    $("button:contains('Generate Forms')").click();
  }
}

function fillOutOne(index, data, settings) {
  if (!settings) {
    $("#link" + index).val("");
  	$("#redeem" + index).html("");
    $("#toko" + index).val("");
    $("#b" + index).prop('checked', false);
    $("#ss" + index).prop('checked', false);
    $("#t" + index).val("").prop('selected', true);
    $("#dvImport" + index).css("display", "none");
    $("#re" + index).prop('checked', false);
    return;
  }
  $("#link" + index).val(data.link);
  $("#redeem" + index).html(data.items);
  $("#redeem" + index).trigger("change");
  // check for auto-redeems
	// check for traits/companions/totems
  if (data.trait === "") {
    $("#t" + index).val("").prop('selected', true);
    $("#toko" + index).val("");
  } else if (settings[data.trait].includes(data.tokoId)) {
    console.log("they're traited, ID " + data.tokoId);
    $("#dvImport" + index).css('display', 'block');
    $("#b" + index).prop('checked', true);
    $("#t" + index).val(data.trait).prop('selected', true);
    $("#toko" + index).val(data.tokoId);
  }
  // check skill seeker
  if (data.items.includes("Trait Token") && settings.SkillSeeker.includes(data.tokoId)) {
    console.log("skill seeker!" + index);
    $("#ss" + index).prop('checked', true);
    $("#toko" + index).val(data.tokoId);
    $("#dvImport" + index).css('display', 'block');
    $("#b" + index).prop('checked', true);
  }
  // check RE double
  if (data.re === 2) {
    $("#re" + index).prop('checked', true);
  }
}

function getSettings() {
  const settings = {
    redeemRe: [-1, 2],
    autogen: false,
    Caver: ["19478","42959", "31164", "31627"],
    Diver: ["25181", "33540", "58957"],
    Hunter: ["33540", "30426"],
    Explorer: ["24949", "33540","35964"],
    Fisher: ["30832"],
    SkillSeeker: ["31030"]
  };
  return settings;
}
// get settings (to-do: from file)


// snippet from https://html-online.com/articles/select-div-content-text-one-click-javascript/

function selectText(containerid) {
    if (document.selection) {
        let range = document.body.createTextRange();
        range.moveToElementText(document.getElementById(containerid));
        range.select();
    } else if (window.getSelection) {
        let range = document.createRange();
        range.selectNode(document.getElementById(containerid));
        window.getSelection().addRange(range);
    }
}